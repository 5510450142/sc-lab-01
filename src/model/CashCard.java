package model;

public class CashCard {
	private int balance;
	

	public CashCard() {
		balance=0;
	}

	public String toString() {
		return "Balance = " + this.balance + "\n";
	}
	
	public int getBalance() {
		return balance;
	}
	
	public void setBalance(int balance) {
		this.balance = balance;
	}

}
