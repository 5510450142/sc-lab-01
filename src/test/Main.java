package test;

import gui.Frame;
import model.CashCard;
import model.CashMachine;

public class Main {

	public static void main(String[] args) {
		Frame frame = new Frame();
		frame.setSize(400, 400);
		CashCard card = new CashCard();
		CashMachine machine = new CashMachine();
		
		machine.insert(card);
		frame.setResult(card.toString());
		
		frame.extendResult("Add 200 ");
		machine.add(200);
		frame.extendResult(card.toString());
		
		frame.extendResult("withDraw 150 ");
		machine.withDraw(150);
		frame.extendResult(card.toString());
		
		frame.extendResult("Add 200 ");
		machine.add(200);
		frame.extendResult(card.toString());
		
		frame.extendResult("withDraw 150 ");
		machine.withDraw(150);
		frame.extendResult(card.toString());
		
		machine.OutCard();
		frame.extendResult(card.toString());
		
		frame.setVisible(true);
		
		
	}

}
